return {
	"tpope/vim-fugitive",
	config = function()
		vim.keymap.set("n", "<leader>gs", vim.cmd.Git, { desc = "Git status" })
		vim.keymap.set("n", "<leader>gd", ":Gvdiff<CR>", { desc = "Git Diff" })
		vim.keymap.set("n", "<leader>gb", ":Git blame<CR>", { desc = "Git Blame" })
	end,
}
