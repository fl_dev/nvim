return {
	{

		"navarasu/onedark.nvim",
		priority = 1000, -- make sure to load this before all the other start plugins
		config = function()
			-- Lua
			require("onedark").setup({
				style = "cool",
			})

			-- load the colorscheme here
			vim.cmd([[colorscheme onedark]])
		end,
	},
}
