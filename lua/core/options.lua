local opt = vim.opt

opt.number = true -- adds line numbers

opt.clipboard = "unnamedplus" -- uses system clipboard

opt.ignorecase = true -- ignores case when searching
opt.smartcase = true -- doesn't ignore case if you use mixed case

opt.wrap = false -- disables line wrapping

opt.cursorline = false -- puts a line under the cursor if true

opt.undofile = true -- writes undo history to file

opt.signcolumn = "yes" -- shows sign column so that text doesn't shift

opt.mouse = "a"

-- decreasing update time
opt.updatetime = 250
opt.timeoutlen = 300

opt.splitright = true -- split vertical window to the right
opt.splitbelow = true -- split horizontal window to the bottom

opt.scrolloff = 10 -- minimum number of screenlines to keep above or below cursor

opt.hlsearch = true -- highlight on search

vim.o.completeopt = "menuone,noselect"
